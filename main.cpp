

//0 is false
//other true
#include "GeneticAlgorithm.h"

int main() {
    std::chrono::steady_clock::time_point begin = std::chrono::steady_clock::now();

    std::cout << "MODYFIKACJA START" << std::endl;
    Tree tree(0);
    std::cout << tree.getPrefix() << std::endl;
    tree.vMutateSaveStruct();
    std::cout << tree.getPrefix() << std::endl;
    std::cout << "MODYFIKACJA END" << std::endl;

    GeneticAlgorithm ga;
    ga.start();

    std::chrono::steady_clock::time_point end = std::chrono::steady_clock::now();

    std::cout << sTIME_ELAPSED
              << std::chrono::duration_cast<std::chrono::microseconds>(end - begin).count() / MICRO_TO_NORMAL << std::endl;

    return 0;
}