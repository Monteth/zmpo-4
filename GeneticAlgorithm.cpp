//
// Created by monteth on 12/6/17.
//


#include "GeneticAlgorithm.h"

void select(std::vector<Tree> &population, std::vector<Tree> &newPopulation);

void GeneticAlgorithm::start() {
    srand(time(NULL));

    double *arrayX;
    double *arrayY;
    double *arrayResult;
    int nrOfProperRecords = 0;

    std::vector<Tree> resultsVector;

    getFileInput(arrayX, arrayY, arrayResult, nrOfProperRecords);

    if (nrOfProperRecords > 0) {
        std::vector<Tree> population;
        std::vector<Tree> tempPopulation;

        init(population);

        for (int m = 0; m < NUMBER_OF_ITERATIONS; ++m) {
            std::cout << sITERATION << m << std::endl;

            select(population, tempPopulation, arrayX, arrayY, arrayResult, nrOfProperRecords);

            crossing(population, tempPopulation, nrOfProperRecords);

            mutate(population);

            Tree resultTree = getBestTree(population, arrayX, arrayY, arrayResult, nrOfProperRecords);

            resultsVector.push_back(resultTree);
            std::cout << sFUN_RESULT << resultTree.getPrefix() << std::endl;
            std::cout << sEVALUATION << resultTree.getEval(arrayX, arrayY, arrayResult, nrOfProperRecords) << std::endl;

        }
        saveResult(resultsVector, arrayX, arrayY, arrayResult, nrOfProperRecords);
    }

}

void GeneticAlgorithm::select(std::vector<Tree> &population, std::vector<Tree> &newPopulation, double *arrayX, double *arrayY, double *arrayResult, int nrOfProperRecords) {
    for (int k = 0; k < POPULATION_SIZE; ++k) {
        int index1 = rand() % POPULATION_SIZE;
        int index2 = rand() % POPULATION_SIZE;
        if (population.at(index1).getEval(arrayX, arrayY, arrayResult, nrOfProperRecords) <
            population.at(index2).getEval(arrayX, arrayY, arrayResult, nrOfProperRecords)) {
            newPopulation.push_back(population.at(index1));
        } else {
            newPopulation.push_back(population.at(index2));
        }
    }//SELEKCJA END
    population.clear();
}

void GeneticAlgorithm::init(std::vector<Tree> &population) {
    for (int i = 0; i < POPULATION_SIZE; ++i) {
        population.push_back(Tree(0));
    }
}

void GeneticAlgorithm::crossing(std::vector<Tree> &population, std::vector<Tree> &newPopulation, int &nrOfProperRecords) {
    for (int l = 0; l < POPULATION_SIZE / 2; ++l) {
        Tree tree1;
        Tree tree2;
        bool gotFst = false;
        for (int i = 0; i < 2; ++i) {
            int index = rand() % newPopulation.size();
            if (!gotFst) {
                tree1 = newPopulation.at(index);
                gotFst = true;
            } else
                tree2 = newPopulation.at(index);
            newPopulation.erase(newPopulation.begin() + index);
        }
        if (rand() % MAX_PROBABILITY < CROSSING_PROBABILITY) {
            Tree tree1Children(tree1);
            Tree tree2Children(tree2);
            tree1Children.crossWith(tree2Children);
            population.push_back(tree1Children);
            population.push_back(tree2Children);
        } else {
            population.push_back(tree1);
            population.push_back(tree2);
        }
    }
}

void GeneticAlgorithm::mutate(std::vector<Tree> &population) {
    for (int i = 0; i < POPULATION_SIZE; ++i) {
        if (rand() % MAX_PROBABILITY < MUTATION_PROBABILITY) {
            population.at(i).mutation();
        }
    }

}

Tree  GeneticAlgorithm::getBestTree(std::vector<Tree> &population, double *arrayX, double *arrayY, double *arrayResult, int &nrOfProperRecords) {
    int resultTreeIndex = 0;
    double bestEval = population.at(0).getEval(arrayX, arrayY, arrayResult, nrOfProperRecords);
    for (int j = 0; j < population.size(); ++j) {
        double currentEval = population.at(j).getEval(arrayX, arrayY, arrayResult, nrOfProperRecords);
        if(bestEval > currentEval);
        resultTreeIndex = j;
        bestEval = currentEval;
    }

    return population.at(resultTreeIndex);
}

void GeneticAlgorithm::saveResult(std::vector<Tree> resultVector, double *arrayX, double *arrayY, double *arrayResult, int& nrOfProperRecords) {
    std::fstream output;
    output.open(sOUTPUT_FILE_NAME, std::ios::out);
    if (output.good()){
        for (int i = 0; i < resultVector.size(); ++i) {
            output << resultVector.at(i).getEval(arrayX, arrayY, arrayResult, nrOfProperRecords) <<
                     "       " << resultVector.at(i).getPrefix()  << std::endl;
        }
    }
    output.close();
}

void GeneticAlgorithm::getFileInput(double *&arrayX, double *&arrayY, double *&arrayResult, int &nrOfProperRecords) {
    std::fstream file;
    file.open(sINPUT_FILE_NAME, std::fstream::in);
    std::string line;
    if (file.good()) {
        int fileSize = 0;
        std::string line;
        while (getline(file, line))
            fileSize++;

        file.clear();
        file.seekg(0, std::ios::beg);

        arrayX = new double[fileSize];
        arrayY = new double[fileSize];
        arrayResult = new double[fileSize];

        while (getline(file, line)) {
            std::string *lineArray;
            bool isDouble = true;
            int nrOfItems = 0;

            lineArray = Tools::split(line, &nrOfItems);

            for (int i = 0; i < nrOfItems; i++) {
                if (!Tools::isDouble(lineArray[i])) {
                    isDouble = false;
                }
            }

            if (isDouble) {
                arrayX[nrOfProperRecords] = stod(lineArray[0]);
                arrayY[nrOfProperRecords] = stod(lineArray[1]);
                arrayResult[nrOfProperRecords] = stod(lineArray[2]);
                nrOfProperRecords++;
            }
        }
    }
    file.close();
}