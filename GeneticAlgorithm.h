//
// Created by monteth on 12/6/17.
//
#include <iostream>
#include <chrono>
#include <vector>
#include <fstream>
#include "Tree/Tools.h"
#include "Tree/Tree.h"
#include "Values.h"

#ifndef ZMPO_4B_GENETICALGORITHM_H
#define ZMPO_4B_GENETICALGORITHM_H


class GeneticAlgorithm {

public:
    void start();

private:
    void select(std::vector<Tree> &population, std::vector<Tree> &newPopulation, double *arrayX, double *arrayY, double *arrayResult, int nrOfProperRecords);
    void init(std::vector<Tree> &vector);
    void crossing(std::vector<Tree> &vector, std::vector<Tree> &population, int &records);
    void mutate(std::vector<Tree> &population);
    Tree getBestTree(std::vector<Tree> &population, double *arrayX, double *arrayY, double *arrayResult, int &nrOfProperRecords);
    void saveResult(std::vector<Tree> resultVector, double *arrayX, double *arrayY, double *arrayResult, int& nrOfProperRecords);
    void getFileInput(double *&arrayX, double *&arrayY, double *&arrayResult, int &nrOfProperRecords);
};



#endif //ZMPO_4B_GENETICALGORITHM_H
