//
// Created by monteth on 12/4/17.
//

#include <iostream>
#include "Tree.h"

Tree::Tree() {
    this->root = new Node();
}

Tree::Tree(int x) {
    this->root = new Node(x);
}

Tree::Tree(const Tree &otherTree) {
    this->root = new Node(*otherTree.root);
}

Tree::~Tree() {
    delete root;
}

double Tree::countResult(double x, double y) const {
    return root->countResult(x, y);
}

std::string Tree::getPrefix() const {
    return root->getPrefix();
}

double Tree::getEval(double *arrayX, double *arrayY, double *arrayResult, int arrayLength) {
    double sumOfPart = 0;
    for (int i = 0; i < arrayLength; ++i) {
        sumOfPart += pow(arrayResult[i] - countResult(arrayX[i], arrayY[i]), 2);
    }
    return sumOfPart;
}

void Tree::mutation() {
    int thisNodeNumber = this->getNumberOfNodes();
    int joinIndex = rand() % thisNodeNumber;
    root->mutation(joinIndex);
}

int Tree::getNumberOfNodes() const {
    return this->root->getNumberOfNodes();
}


void Tree::operator=(const Tree &otherTree) {
    delete this->root;
    this->root = new Node(*otherTree.root);
}

void Tree::crossWith(Tree &tree2) {
    if (this->root->quantOfChildren != 0 && tree2.root->quantOfChildren != 0) {
        int indexThis = rand() % this->root->quantOfChildren;
        int indexOther = rand() % tree2.root->quantOfChildren;
        Node *temp = this->root->children[indexThis];
        this->root->children[indexThis] = tree2.root->children[indexOther];
        tree2.root->children[indexOther] = temp;
    }
}

void Tree::vMutateSaveStruct() {
    this->root->vMutateSaveStruct();
}
