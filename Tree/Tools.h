//
// Created by monteth on 12/4/17.
//

#ifndef ZMPO_4B_TOOLS_H
#define ZMPO_4B_TOOLS_H


#include <string>

class Tools {
public:
    static int getQuantOfChildren(std::string inputString);
    static bool isOperator(std::string inputString);
    static bool isDouble(std::string inputString);
    static void push(std::string *&array, int length, std::string newWord);
    static std::string *split(std::string str, int *length);
};


#endif //ZMPO_4B_TOOLS_H
