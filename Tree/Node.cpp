//
// Created by monteth on 12/4/17.
//

#include "Node.h"
#include "Tools.h"
#include "../Values.h"
#include <cmath>
#include <iostream>

Node::Node() {
    this->value = "";
    this->quantOfChildren=0;
    this->children = new Node*();
}

Node::Node(int x) {
    this->value = getRandomNodeValue();
    this->quantOfChildren = Tools::getQuantOfChildren(this->value);
    children = new Node*[quantOfChildren];
    for (int i = 0; i < quantOfChildren; ++i) {
        children[i] = new Node(x);
    }
}

Node::Node(const Node &otherNode) {
    this->value = otherNode.value;
    quantOfChildren = otherNode.quantOfChildren;
    children = new Node*[quantOfChildren];
    for (int i = 0; i < quantOfChildren; ++i) {
        children[i] = new Node(*otherNode.children[i]);
    }
}

Node::~Node() {
    for (int i = 0; i < quantOfChildren; ++i) {
        delete children[i];
    }
    delete []children;
}

double Node::countResult(double x, double y) {
    double result = 0;
    if (Tools::isDouble(this->value)){
        result = stod(this->value);
    }else if(this->value == varX) {
        result = x;
    }else if(this->value == varY) {
        result = y;
    }else if(Tools::isOperator(this->value)){
        if (this->value == opADD){
            result = children[0]->countResult(x,y) + children[1]->countResult(x,y);
        }else if (this->value == opSUB){
            result = children[0]->countResult(x,y) - children[1]->countResult(x,y);
        }else if (this->value == opMUL){
            result = children[0]->countResult(x,y) * children[1]->countResult(x,y);
        }else if (this->value == opDIV){  //if /0 result = 0
            if(children[1]->countResult(x,y) != 0)
                result = children[0]->countResult(x,y) / children[1]->countResult(x,y);
        }else if (this->value == opSIN){
            result = sin(children[0]->countResult(x,y));
        }else if (this->value == opCOS){
            result = cos(children[0]->countResult(x,y));
        }
    }
    return result;
}

std::string Node::getRandomNodeValue() {
    std::string result;
    if((rand() % MAX_PROBABILITY) < VALUE_PROBABILITY)
        result = getRandomValue();
    else
        result = getRandomOperator();

    return result;
}

std::string Node::getRandomOperator() {
    std::string operators[6] = {opADD, opSUB, opMUL, opDIV, opSIN, opCOS};
    int index = rand() % 6;
    return operators[index];
}

std::string Node::getRandomValue() {
    std::string result = "";
    int random = rand() % 5;
    switch (random){
        case 0:
        case 1: result = std::to_string((rand() % RANDOM_VALUE_RANGE) - RANDOM_VALUE_RANGE/2);
            break;
        case 2: result = std::to_string((rand() % RANDOM_VALUE_RANGE) - RANDOM_VALUE_RANGE/2 + (double)rand() / RAND_MAX);
            break;
        case 3: result = varX;
            break;
        case 4: result = varY;
            break;
        default:
            result = std::to_string((rand() % RANDOM_VALUE_RANGE) - RANDOM_VALUE_RANGE/2);
    }
    return result;
}

std::string Node::getPrefix() {
    std::string result = "";
    result += this->value + "  ";
    for (int i = 0; i < quantOfChildren; ++i) {
        result += children[i]->getPrefix();
    }
    return result;
}

int Node::getNumberOfNodes() {
    int result = 1;
    for (int i = 0; i < quantOfChildren; ++i) {
        result += children[i]->getNumberOfNodes();
    }
    return result;
}

void Node::mutation(int &nodeNumber) {
    if (nodeNumber == 0){
        if (rand()%MAX_PROBABILITY < SWICHING_IN_MUTATION_PROBABILITY && this->quantOfChildren == 2){
            Node *buf = children[0];
            children[0] = children[1];
            children[1] = buf;
        }else{
            this->value = getRandomNodeValue();
            this->quantOfChildren = Tools::getQuantOfChildren(this->value);
            for (int i = 0; i < quantOfChildren; ++i) {
                children[i] = new Node(0);
            }
        }
    }
    else{
        nodeNumber--;
        for (int i = 0; i < quantOfChildren; ++i) {
            children[i]->mutation(nodeNumber);
        }
    }
}

void Node::join(int &nodeNumber, const Node &node) {
    if (nodeNumber == 0){
        nodeNumber--;
        *this = node;
    } else{
        nodeNumber--;
        for (int i = 0; i < quantOfChildren; ++i) {
            children[i]->join(nodeNumber, node);
        }
    }
}

void Node::getNodeAt(int &index, Node &resultNode){
    if (index == 0){
        resultNode = *this;
        --index;
    }else{
        --index;
        for (int i = 0; i < quantOfChildren; ++i) {
            children[i]->getNodeAt(index, resultNode);
        }
    }
}

void Node::operator=(const Node &node) {
    for (int i = 0; i < quantOfChildren; ++i) {
        delete children[i];
    }
    delete children;
    this->value = node.value;
    this->quantOfChildren = node.quantOfChildren;
    for (int j = 0; j < quantOfChildren; ++j) {
        this->children[j] = node.children[j];
    }
}

Node * Node::getRandomNode() {
    if (quantOfChildren == 1) {
        if (children[0]->quantOfChildren > 0 && rand() % 2) {
            children[0]->getRandomNode();
        } else return this;
    } else if (quantOfChildren == 2) {
        if (children[0]->quantOfChildren > 0 && rand() % 2) {
            children[0]->getRandomNode();
        } else if (children[1]->quantOfChildren > 0 && rand() % 2) {
            children[1]->getRandomNode();
        } else return this;
    } else return this;
}

void Node::vMutateSaveStruct() {
    std::string oldValue = this->value;
    if (Tools::isOperator(oldValue)){
        if (oldValue == opSIN){
            this->value = opCOS;
        }else if (oldValue == opCOS){
            this->value = opSIN;
        } else {
            while (this->value == oldValue) {
                this->value = getRandomOperator();
            }
        }
    } else if(oldValue == varX){
        this->value = varY;
    } else if(oldValue == varY){
        this->value = varX;
    }else if (Tools::isDouble(oldValue)){
        while (this->value == oldValue){
            this->value = std::to_string((rand() % RANDOM_VALUE_RANGE) - RANDOM_VALUE_RANGE/2 + (double)rand() / RAND_MAX);
        }
    }

    for (int i = 0; i < quantOfChildren; ++i) {
        children[i]->vMutateSaveStruct();
    }
}
