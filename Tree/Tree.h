//
// Created by monteth on 12/4/17.
//

#ifndef ZMPO_4B_TREE_H
#define ZMPO_4B_TREE_H


#include "Node.h"
#include <cmath>

class Tree {
public:
    Tree();
    Tree(const Tree &otherTree);
    Tree(int x);
    ~Tree();
    void mutation();
    double countResult(double x, double y) const;
    std::string getPrefix() const;
    int getNumberOfNodes() const;
    void crossWith(Tree &tree2);
    void operator=(const Tree & otherTree);
    double getEval(double *arrayX, double *arrayY, double *arrayResult, int arrayLength);
    void vMutateSaveStruct();
private:
    Node *root;
};


#endif //ZMPO_4B_TREE_H
