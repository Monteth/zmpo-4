//
// Created by monteth on 12/1/17.
//


#include "Tools.h"
#include "../Values.h"

#include <random>

bool Tools::isDouble(std::string inputString) {
    try{
        std::stod(inputString);
    }catch(...){
        return false;
    }
    return true;
}

bool Tools::isOperator(std::string inputString) {
    bool result = false;
    if (inputString == opADD ||
        inputString == opSUB ||
        inputString == opMUL ||
        inputString == opDIV ||
        inputString == opSIN ||
        inputString == opCOS) {
        result = true;
    }
    return result;
}

int Tools::getQuantOfChildren(std::string inputString) {
    int result = 0;
    if (isOperator(inputString)) {
        if (inputString == opADD
            || inputString == opSUB
            || inputString == opMUL
            || inputString == opDIV) {
            result = 2;
        } else if (inputString == opSIN
                   || inputString == opCOS) {
            result = 1;
        }
    }

    return result;
}

std::string *Tools::split(std::string str, int *length) {
    char separator[2] = {' ', ';'};
    std::string *arrayOfWords;
    arrayOfWords = new std::string[1];
    std::string word;

    for (int i = 0; i <= str.length(); i++) {
        if ((i == str.length() || str[i] == separator[0] || str[i] == separator[1]) && word.length() > 0) {
            push(arrayOfWords, *length, word);
            (*length)++;
            word = "";
        } else if (str[i] != separator[0] && str[i] != separator[1]) {
            word += str[i];
        }
    }
    return arrayOfWords;
}

void Tools::push(std::string *&array, int length, std::string newWord) {
    std::string *newArray;
    newArray = new std::string[length + 1];

    for (int i = 0; i < length; i++) {
        newArray[i] = array[i];
    }

    newArray[length] = move(newWord);
    delete[] array;
    array = newArray;
}