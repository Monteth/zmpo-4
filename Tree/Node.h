//
// Created by monteth on 12/4/17.
//

#ifndef ZMPO_4B_NODE_H
#define ZMPO_4B_NODE_H


#include <string>

class Node {
public:
    Node();
    Node(const Node &otherNode);

    Node(int x);

    ~Node();

    int getNumberOfNodes();
    double countResult(double x, double y);
    static std::string getRandomOperator();
    void join(int &nodeNumber, const Node &node);
    void mutation(int &nodeNumber);
    std::string getPrefix();
    void getNodeAt(int &index, Node &resultNode);
    void operator=(const Node& node);
    void vMutateSaveStruct();
private:
    std::string value;
    Node **children;
    int quantOfChildren;
    friend class Tree;

    static std::string getRandomNodeValue();
    static std::string getRandomValue();
    Node * getRandomNode();
};


#endif //ZMPO_4B_NODE_H
