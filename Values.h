//
// Created by monteth on 12/5/17.
//

#ifndef ZMPO_4B_VALUES_H
#define ZMPO_4B_VALUES_H

#include <string>

const int POPULATION_SIZE = 5000;       //500   //1000
const int NUMBER_OF_ITERATIONS = 50;   //200   //20
const int CROSSING_PROBABILITY = 15;    //0-MAX_PROBABILITY
const int MUTATION_PROBABILITY = 15;    //0-MAX_PROBABILITY
const int VALUE_PROBABILITY = 80;    //0-MAX_PROBABILITY // real 50-100
const int SWICHING_IN_MUTATION_PROBABILITY = 0;    //0-MAX_PROBABILITY // real 50-100
const int MAX_PROBABILITY = 100;
const int RANDOM_VALUE_RANGE = 200;
const int MICRO_TO_NORMAL = 1000000;    //0-100


const std::string opADD = "+";
const std::string opSUB = "-";
const std::string opMUL = "*";
const std::string opDIV = "/";
const std::string opSIN = "sin";
const std::string opCOS = "cos";
const std::string varX = "x";
const std::string varY = "y";


const std::string sINPUT_FILE_NAME = "input.txt";
const std::string sOUTPUT_FILE_NAME = "output.txt";
const std::string sFUN_RESULT = "f(x, y) = ";
const std::string sEVALUATION = "Ewaluacja: ";
const std::string sITERATION = "Iteracja nr: ";
const std::string sTIME_ELAPSED = "Elapsed time: ";

#endif //ZMPO_4B_VALUES_H
